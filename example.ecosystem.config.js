module.exports = {
  apps : [{
    name   : 'Rent A Car App',
    script : './dist/ssr_live/index.js',
    env: {
      NODE_ENV: 'development',
      APP_PORT: 3000,
      APP_NAME: 'Rent A Car Web App',
      APP_SHORTNAME: 'Rent A Car App',
      APP_DESCRIPTION: 'Rent A Car Web App Customer Interface',
      APP_DOMAIN: 'racw.cloc',
      APP_URL: 'http://racw.cloc',
      API_URL: 'http://api.racw.cloc',
      CDN_URL: 'http://cdn.racw.cloc',
      GTAG_ID: null,
      GTN_ID: null,
      PHONE_NUMBER: '+90 850 000 00 00',
      ADDRESS: 'Türkiye',
      TAWK_PROPERTY_ID: '',
      TAWK_WIDGET_ID: ''
    },
    env_production: {
      NODE_ENV: 'production',
      APP_PORT: 3000,
      APP_NAME: 'Rent A Car Web App',
      APP_SHORTNAME: 'Rent A Car App',
      APP_DESCRIPTION: 'Rent A Car Web App Customer Interface',
      APP_DOMAIN: 'carrental.com',
      APP_URL: 'https://carrental.com',
      API_URL: 'https://api.carrental.com',
      CDN_URL: 'https://cdn.carrental.com',
      GTAG_ID: null,
      GTN_ID: null,
      PHONE_NUMBER: '+90 850 000 00 00',
      ADDRESS: 'Türkiye',
      TAWK_PROPERTY_ID: '',
      TAWK_WIDGET_ID: ''
    }
  }]
}
