class WebpackPwaEnvPlugin {
  apply(compiler) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
    compiler.hooks.done.tap(
      'Prepare PWA Env Variables Plugin',
      () => {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const fs = require('fs');

        if (!fs.existsSync('./src/helpers/pwa-env.js')) {
          fs.writeFile('./src/helpers/pwa-env.js', '');
        }

        const keys = [
          'NODE_ENV',
          'APP_PORT',
          'APP_NAME',
          'APP_SHORTNAME',
          'APP_DESCRIPTION',
          'APP_DOMAIN',
          'APP_URL',
          'API_URL',
          'CDN_URL',
          'GTAG_ID',
          'GTM_ID',
          'PHONE_NUMBER',
          'ADDRESS',
          'TAWK_PROPERTY_ID',
          'TAWK_WIDGET_ID',
        ];
        let rows = [];

        keys.forEach((key) => {
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          rows.push(`  ${key}: '${process.env.hasOwnProperty(key) ? process.env[key] : ''}'`);
        });

        fs.writeFileSync('./src/helpers/pwa-env.js',
          `export default {
${rows.join(',\n')},
}
`);

//         fs.writeFileSync('./src/helpers/pwa-env.js',
//           `export default {
//   NODE_ENV: '${process.env.NODE_ENV}',
//   APP_PORT: ${process.env.APP_PORT},
//   APP_NAME: '${process.env.APP_NAME}',
//   APP_SHORTNAME: '${process.env.APP_SHORTNAME}',
//   APP_DESCRIPTION: '${process.env.APP_DESCRIPTION}',
//   APP_DOMAIN: '${process.env.APP_DOMAIN}',
//   APP_URL: '${process.env.APP_URL}',
//   API_URL: '${process.env.API_URL}',
//   CDN_URL: '${process.env.CDN_URL}',
//   GTAG_ID: '${process.env.GTAG_ID}',
//   PHONE_NUMBER: '${process.env.PHONE_NUMBER}',
//   ADDRESS: '${process.env.ADDRESS}'
// }
// `);
      }
    );
  }
}

module.exports = { WebpackPwaEnvPlugin };
