import { useQuasar } from 'quasar';
import { computed } from 'vue';
import { useStore } from 'src/store';
import { DriverInfoReservationInterface } from 'src/models/makeReservation';

export default function auth() {
  const $q = useQuasar();
  const store = useStore();

  const validateExtra = (): boolean => {
    void store.dispatch('makeReservation/setExtraStepIsValid', true);

    return true;
  }

  const validateDriver = (): boolean => {
    let status = true;
    const driverInfo = computed((): DriverInfoReservationInterface | null => store.state.makeReservation.driverInfo);

    if (driverInfo.value?.firstName === null || driverInfo.value?.firstName === '') {
      status = false;
      $q.notify({
        color: 'negative',
        position: 'top-right',
        message: 'Sürücü adı gereklidir',
        icon: 'report_problem'
      });
    }

    if (driverInfo.value?.lastName === null || driverInfo.value?.lastName === '') {
      status = false;
      $q.notify({
        color: 'negative',
        position: 'top-right',
        message: 'Sürücü soyadı gereklidir',
        icon: 'report_problem'
      });
    }

    if (driverInfo.value?.email === null || driverInfo.value?.email === '') {
      status = false;
      $q.notify({
        color: 'negative',
        position: 'top-right',
        message: 'E-Posta gereklidir',
        icon: 'report_problem'
      });
    } else {
      const emailFilterExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      const email = <string> driverInfo.value?.email;

      if (!emailFilterExp.test(email.toLowerCase())) {
        status = false;
        $q.notify({
          color: 'negative',
          position: 'top-right',
          message: 'E-Posta geçersiz',
          icon: 'report_problem'
        });
      }
    }

    if (driverInfo.value?.phone === null || driverInfo.value?.phone === '') {
      status = false;
      $q.notify({
        color: 'negative',
        position: 'top-right',
        message: 'Telefon gereklidir',
        icon: 'report_problem'
      });
    }

    if (driverInfo.value?.country === null) {
      status = false;
      $q.notify({
        color: 'negative',
        position: 'top-right',
        message: 'Ülke gereklidir',
        icon: 'report_problem'
      });
    }

    if (driverInfo.value?.licenseAge === null || driverInfo.value?.licenseAge === '') {
      status = false;
      $q.notify({
        color: 'negative',
        position: 'top-right',
        message: 'Ehliyet yaşı gereklidir',
        icon: 'report_problem'
      });
    }

    if (driverInfo.value?.birthDate.day === null || driverInfo.value?.birthDate.month === null || driverInfo.value?.birthDate.year === null) {
      status = false;
      $q.notify({
        color: 'negative',
        position: 'top-right',
        message: 'Doğum tarihi gereklidir',
        icon: 'report_problem'
      });
    }

    void store.dispatch('makeReservation/setDriverStepIsValid', status);

    return status;
  }

  const validatePayment = (): boolean => {
    void store.dispatch('makeReservation/setPaymentStepIsValid', true);

    return true;
  }

  const validateAll = (): boolean => {
    return validateExtra() && validateDriver() && validatePayment();
  }

  return {
    validateExtra,
    validateDriver,
    validatePayment,
    validateAll
  };
}
