// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import pwaEnv from '../helpers/pwa-env';
import ProcessEnv = NodeJS.ProcessEnv;
import { useMeta } from 'quasar';

export default function helpers() {
  const cdnUrl = (path: null | string = null): string => {
    // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
    let url = env('CDN_URL') +  '/';

    if (path !== null) {
      url += path;
    }

    return url;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const env = (key: string, value: null | string = null): any => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore, @typescript-eslint/no-unsafe-assignment
    const processEnv: ProcessEnv = typeof process === typeof undefined ? pwaEnv : process.env;

    // if (typeof process === typeof undefined) {
    //   return value;
    // }
    //
    // const processEnv = process.env;

    if (typeof processEnv[key] !== typeof undefined) {
      return processEnv[key];
    }

    return value;
  }

  const envStr = (key: string, value: null | string = null): string => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return env(key, value);
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  const updateMeta = (params: object): void => {
    const data = {};

    if (params.hasOwnProperty('title')) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/ban-ts-comment
      // @ts-ignore
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      data.title = params.title;
    }

    if (params.hasOwnProperty('description') || params.hasOwnProperty('keywords')) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/ban-ts-comment
      // @ts-ignore
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      data.meta = {};

      if (params.hasOwnProperty('description')) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/ban-ts-comment
        // @ts-ignore
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-member-access
        data.meta.description = { name: 'description', content: params.description };
      }

      if (params.hasOwnProperty('keywords')) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/ban-ts-comment
        // @ts-ignore
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-member-access
        data.meta.keywords = { name: 'keywords', content: params.keywords };
      }
    }

    // void store.dispatch('global/setMeta', {
    //   title: apiPage.meta.title,
    //   description: apiPage.meta.description === null ? '' : apiPage.meta.description,
    //   keywords: apiPage.meta.keywords === null ? '' : apiPage.meta.keywords
    // });

    useMeta(() => {
      return data;
    });
  }

  return {
    cdnUrl,
    env,
    envStr,
    updateMeta,
  };
}
