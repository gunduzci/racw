import { boot } from 'quasar/wrappers';
import axios, { AxiosInstance } from 'axios';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
  }
}

const api = axios.create({
  baseURL: process.env.API_URL,
  headers: {
    'X-Endpoint-Domain': process.env.APP_DOMAIN,
    'X-Browser-Token': null,
    'X-Currency-Code': 'EUR',
    'X-App-Locale': 'tr',
  }
});

if (!process.env.SERVER) {
  if (typeof localStorage.authToken !== typeof undefined && localStorage.authToken !== '') {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
    api.defaults.headers.common.Authorization = `Bearer ${localStorage.authToken}`;
  }

  if (typeof localStorage.BrowserToken !== typeof undefined && localStorage.BrowserToken !== '') {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
    api.defaults.headers['X-Browser-Token'] = `${localStorage.BrowserToken}`;
  }

  if (typeof localStorage.CurrencyCode !== typeof undefined && localStorage.CurrencyCode !== '') {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
    api.defaults.headers['X-Currency-Code'] = `${localStorage.CurrencyCode}`;
  }

  if (typeof localStorage.AppLocale !== typeof undefined && localStorage.AppLocale !== '') {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
    api.defaults.headers['X-App-Locale'] = `${localStorage.AppLocale}`;
  }
}

const setBrowserToken = (token: string) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  api.defaults.headers['X-Browser-Token'] = token;
}

const setCurrencyCode = (currencyCode: string) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  api.defaults.headers['X-Currency-Code'] = currencyCode;
}

const setAppLocale = (appLocale: string) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  api.defaults.headers['X-App-Locale'] = appLocale;
}

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
});

export { axios, api, setBrowserToken, setCurrencyCode, setAppLocale };
