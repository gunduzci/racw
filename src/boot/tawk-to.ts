import { boot } from 'quasar/wrappers'
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import TawkMessengerVue from '@tawk.to/tawk-messenger-vue';

export default boot(({ app}) => {
  app.use(TawkMessengerVue, {
    propertyId : process.env.TAWK_PROPERTY_ID,
    widgetId : process.env.TAWK_WIDGET_ID
  });
})
