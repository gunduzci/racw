import { boot } from 'quasar/wrappers'
import VueGtag from 'vue-gtag';
import { event, pageview } from 'vue-gtag'

export default boot(({ app, router}) => {
  app.use(VueGtag, {
    enabled: process.env.GTAG_ID !== null && process.env.GTAG_ID !== '',
    config: {
      id: process.env.GTAG_ID,
      params: {
        send_page_view: true,
      }
    }
  });

  app.config.globalProperties.event = event;
  app.config.globalProperties.pageview = pageview;

  router.afterEach((to) => {
    // eslint-disable-next-line
    // @ts-ignore
    pageview(to.path);
  });
})

export { event, pageview }
