export default {
  pickupDropPoint: 'Alış / Dönüş Yeri',
  pickupPoint: 'Alış Yeri',
  dropPoint: 'Dönüş Yeri',
  differentPointSwitch: 'Farklı yerde teslim etmek istiyorum',
  pickupDate: 'Alış Tarihi',
  pickupTime: 'Alış Saati',
  dropDate: 'Dönüş Tarihi',
  dropTime: 'Dönüş Saati',
  yesButton: 'Evet',
  noButton: 'Hayır',
};
