import _search from './_search';

export default {
  failed: 'Eylem başarısız',
  success: 'Eylem başarılı oldu',
  search: _search
};
