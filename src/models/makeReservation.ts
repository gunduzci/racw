import { AxiosResponse } from 'axios';

export interface Extra {
  groupId: number;
  id: number;
  code: null | string;
  name: string;
  description: string;
  orderRank: number;
  priceType: string;
  currencyCode: string;
  baseAmount: number;
  calculatedAmount: number;
  minPiece: number;
  maxPiece: number;
  selected: boolean;
}

export interface ExtraReservationInterface {
  id: number;
  name: string;
  description: string;
  piece: number;
  amount: number;
  priceType: string;
}

export interface ExtraGroupListItem {
  id: number;
  name: string;
  items: Extra[];
}
export interface ExtraApiResponseInterface {
  groupId: number;
  id: number;
  name: string;
  code: null | string;
  description: string;
  orderRank: number;
  baseAmount: number;
  calculatedAmount: number;
  currencyCode: string;
  priceType: string;
  minPiece: number;
  maxPiece: number;
}

export interface ExtraGroupApiResponseInterface {
  id: number;
  name: string;
  code: string;
}

export interface AxiosExtrasInterface extends AxiosResponse {
  data: {
    groups: ExtraGroupApiResponseInterface[];
    extras: ExtraApiResponseInterface[];
  }
}

export interface DriverInfoReservationInterface {
  isValid: boolean;
  firstName: string | null;
  lastName: string | null;
  email: string | null;
  phone: string | null;
  country: CountrySelectInterface | null;
  licenseAge: string | null;
  birthDate: {
    day: string | null,
    month: string | null,
    year: string | null,
  };
}

export interface CountrySelectInterface {
  code: string;
  name: string;
}

export interface AxiosCountriesInterface extends AxiosResponse {
  data: CountrySelectInterface[]
}

export interface AxiosRentalStoreInterface extends AxiosResponse {
  data: {
    status: boolean;
    message: null | string;
    payload: {
      code:  null | string;
    };
  }
}

export interface AdditionalAmountsReservationInterface {
  country: number,
  pickupPoint: number,
  dropPoint: number,
  drop: number,
  closerDay: number,
  total: number,
}
