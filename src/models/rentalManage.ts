import { AxiosResponse } from 'axios';

export interface RentalManageInterface {
  code: string;
  driverName: string;
  pickupAt: string;
  pickupPoint: string;
  dropAt: string;
  dropPoint: string;
  rentalDay: number;
  vehicleName: string;
  currency_code: string;
  rentalAmount: number;
  paymentMethodName: string;
  statusName: string;
  extras: string[];
}

export interface AxiosRentalManageInterface extends AxiosResponse {
  data: {
    status: boolean;
    message: null | string;
    payload: RentalManageInterface;
  }
}
