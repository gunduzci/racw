import { AxiosResponse } from 'axios';

export interface DeliveryPoint {
  id: number;
  name: string;
  type: string;
  icon: string;
  search: string;
  slug: null | string;
  orderRank: number;
  isVisible: boolean;
  city: City,
}

export interface City {
  id: number;
  name: string;
  slug: string|null;
  orderRank: number;
}

export interface DeliveryPointListItem {
  isVisible: boolean;
  orderRank: number;
  city: City,
  deliveryPoints: DeliveryPoint[],
}

export interface DeliveryPointApiResponse {
  id: number;
  name: string;
  slug: null | string;
  type: string;
  icon: string;
  search: string;
  orderRank: number;
}

export interface DeliveryPointListItemApiResponse {
  orderRank: number;
  city: City,
  deliveryPoints: DeliveryPointApiResponse[],
}

export interface AxiosDeliveryPointListItemInterface extends AxiosResponse {
  data: DeliveryPointListItemApiResponse[]
}

export interface SearchData {
  pickupPoint: DeliveryPoint,
  dropPoint: DeliveryPoint,
  differentReturnPoint: boolean,
  pickupDateTime: Date,
  pickupDateTimeString: string,
  dropDateTime: Date,
  dropDateTimeString: string,
  rentalDay: number,
}

export interface Page {
  slug: string;
  meta: {
    title: string,
    description: null | string,
    keywords: null | string,
  };
  image: string;
  headerHeight: number;
  title: string;
  caption: null | string;
  content: null | string;
}

export interface SearchPageApiResponseInterface {
  slug: string;
  meta: {
    title: string,
    description: null | string,
    keywords: null | string,
  };
  image: string;
  title: string;
  caption: null | string;
  content: null | string;
  deliveryPointId: null | number;
}

export interface AxiosSearchPageResponseInterface extends AxiosResponse {
  data: SearchPageApiResponseInterface
}

export interface SelectItemInterface {
  id: number;
  name: string;
  search: null | string;
}

export interface VehicleFilterItemsApiResponse {
  brand: SelectItemInterface[];
  group: SelectItemInterface[];
  fuel: SelectItemInterface[];
  transmission: SelectItemInterface[];
}

export interface AxiosVehicleFilterItemsResponseInterface extends AxiosResponse {
  data: VehicleFilterItemsApiResponse
}

export interface VehiclePropertyStar {
  color: string,
  name: string
}

export interface VehiclePropertyReview {
  userName: string,
  content: string,
  country: string,
  rating: number
}

export interface Vehicle {
  id: number;
  name: string;
  image: string|null;
  properties: {
    brand: {
      id: number;
      name: string;
    };
    group: {
      id: number;
      name: string;
    };
    fuel: {
      id: number;
      name: string;
    };
    transmission: {
      id: number;
      name: string;
    };
    luggageVolume: {
      value: number|null;
      name: string|null;
    };
    suitcaseCapacity: {
      value: number|null;
      name: string|null;
    };
  };
  minDriverAge: number;
  minLicenseAge: number;
  orderRank: number;
  calculation: {
    currencyCode: string;
    missing: boolean;
    plain: {
      daily: number;
      total: number;
    }
    total: {
      daily: number;
      total: number;
    },
    deposit: number;
  }
  stars: VehiclePropertyStar[];
  reviews: VehiclePropertyReview[]
}

export interface Extra {
  id: number;
  name: string;
  description: string;
  price: {
    minAmount: number,
    amount: number,
    daily: boolean,
  };
  selected: boolean;
  group: ExtraGroup;
}

export interface ExtraGroup {
  id: number;
  name: string;
}
