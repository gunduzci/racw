import { QSelect } from 'quasar';

export type qSelectDoneFnCallbackFn = () => void;
export type qSelectDoneFnAfterFn = (ref: QSelect) => void;
export type qSelectDoneFn = (callbackFn: qSelectDoneFnCallbackFn, afterFn: qSelectDoneFnAfterFn) => void;
