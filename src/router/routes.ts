import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '/',
        component: () => import('layouts/SearchLayout.vue'),
        children: [
          {
            path: '/',
            name: 'home',
            component: () => import('pages/Home.vue')
          },
          {
            path: 'arac-kiralama',
            name: 'search',
            component: () => import('pages/Search.vue'),
            children: [
              {
                path: ':slug',
                name: 'searchSlug',
                component: () => import('pages/Search.vue')
              }
            ]
          }
        ],
      },
      {
        path: '/rezervasyon-olustur',
        name: 'makeReservation',
        component: () => import('layouts/MakeReservationLayout.vue'),
        children: [
          {
            path: 'ekstra-secimi',
            name: 'makeReservationExtra',
            component: () => import('pages/make-reservation/MakeReservationExtra.vue'),
          },
          {
            path: 'surucu-bilgileri',
            name: 'makeReservationDriver',
            component: () => import('pages/make-reservation/MakeReservationDriver.vue'),
          },
          {
            path: 'odeme-yontemi',
            name: 'makeReservationPayment',
            component: () => import('pages/make-reservation/MakeReservationPayment.vue'),
          },
          {
            path: 'ozet',
            name: 'makeReservationSummary',
            component: () => import('pages/make-reservation/MakeReservationSummary.vue'),
          }
        ],
      },
      {
        path: 'rezervasyon-olusturuldu',
        name: 'makeReservationCompleted',
        component: () => import('pages/make-reservation/MakeReservationCompleted.vue'),
      },
      {
        path: 'rezervasyon-yonetimi',
        name: 'rentalManage',
        component: () => import('pages/RentalManage.vue'),
      },
      {
        path: 'hakkimizda',
        name: 'about',
        component: () => import('pages/About.vue'),
      },
      {
        path: 'kiralama-kosullari',
        name: 'rental-conditions',
        component: () => import('pages/RentalConditions.vue'),
      },
      {
        path: 'iletisim',
        name: 'contact',
        component: () => import('pages/Contact.vue'),
      }
    ],
  },
  {
    path: '/pages',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: 'index', component: () => import('pages/Index.vue') }],
  },
  {
    path: '/test/x',
    name: 'x',
    component: () => import('pages/x.vue'),
  },
  {
    path: '/test/y',
    name: 'y',
    component: () => import('pages/y.vue'),
  },
  {
    path: '/test/z',
    name: 'z',
    component: () => import('pages/z.vue'),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
