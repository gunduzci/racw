declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: string;
    VUE_ROUTER_MODE: 'hash' | 'history' | 'abstract' | undefined;
    VUE_ROUTER_BASE: string | undefined;
    APP_PORT: string;
    APP_NAME: string;
    APP_SHORTNAME: string;
    APP_DESCRIPTION: string;
    APP_DOMAIN: string;
    APP_URL: string;
    API_URL: string;
    CDN_URL: string;
    GTAG_ID: string | null | undefined;
    GTM_ID: string | null | undefined;
  }
}
