import { MutationTree } from 'vuex';
import { MakeReservationStateInterface } from './state';
import { Vehicle } from 'src/models/search';
import { DriverInfoReservationInterface, ExtraReservationInterface } from 'src/models/makeReservation';

const mutation: MutationTree<MakeReservationStateInterface> = {
  setVehicle (state: MakeReservationStateInterface, vehicle: Vehicle | null) {
    state.vehicle = vehicle;
  },
  setExtras (state: MakeReservationStateInterface, extras: ExtraReservationInterface[] | []) {
    state.extras = extras;
  },
  setExtraAmount (state: MakeReservationStateInterface, extraAmount: number) {
    state.extraAmount = extraAmount;
  },
  setDriverInfo (state: MakeReservationStateInterface, driverInfo: DriverInfoReservationInterface) {
    state.driverInfo = driverInfo;
  },
  setPaymentMethod (state: MakeReservationStateInterface, paymentMethod: string) {
    state.paymentMethod = paymentMethod;
  },
  setRentalAmount (state: MakeReservationStateInterface, rentalAmount: number) {
    state.rentalAmount = rentalAmount;
  },
  setExtraStepIsValid (state: MakeReservationStateInterface, status: boolean) {
    state.extraStepIsValid = status;
  },
  setDriverStepIsValid (state: MakeReservationStateInterface, status: boolean) {
    state.driverStepIsValid = status;
  },
  setPaymentStepIsValid (state: MakeReservationStateInterface, status: boolean) {
    state.paymentStepIsValid = status;
  },
  setCompletedData (state: MakeReservationStateInterface, data) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    state.completed = data;
  },
};

export default mutation;
