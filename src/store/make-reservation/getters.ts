import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { MakeReservationStateInterface } from './state';

const getters: GetterTree<MakeReservationStateInterface, StateInterface> = {
  allowStepDriver: (state: MakeReservationStateInterface): boolean => {
    return state.extraStepIsValid;
  },
  allowStepPayment: (state: MakeReservationStateInterface): boolean => {
    return state.extraStepIsValid && state.driverStepIsValid;
  },
  allowStepSummary: (state: MakeReservationStateInterface): boolean => {
    return state.extraStepIsValid && state.driverStepIsValid && state.paymentStepIsValid;
  }
};

export default getters;
