import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { MakeReservationStateInterface } from './state';
import { Vehicle } from 'src/models/search';
import { DriverInfoReservationInterface, ExtraReservationInterface } from 'src/models/makeReservation';

const actions: ActionTree<MakeReservationStateInterface, StateInterface> = {
  setVehicle ({ commit }, vehicle: Vehicle | null) {
    commit('setVehicle', vehicle);
  },
  setExtras ({ commit }, extras: ExtraReservationInterface[] | []) {
    commit('setExtras', extras);
  },
  setExtraAmount ({ commit }, extraAmount: number) {
    commit('setExtraAmount', extraAmount);
  },
  setDriverInfo ({ commit }, driverInfo: DriverInfoReservationInterface) {
    commit('setDriverInfo', driverInfo);
  },
  setPaymentMethod ({ commit }, paymentMethod: string) {
    commit('setPaymentMethod', paymentMethod);
  },
  setRentalAmount ({ commit }, rentalAmount: number) {
    commit('setRentalAmount', rentalAmount);
  },
  setExtraStepIsValid ({ commit }, status: boolean) {
    commit('setExtraStepIsValid', status);
  },
  setDriverStepIsValid ({ commit }, status: boolean) {
    commit('setDriverStepIsValid', status);
  },
  setPaymentStepIsValid ({ commit }, status: boolean) {
    commit('setPaymentStepIsValid', status);
  },
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setCompletedData ({ commit }, data: any) {
    commit('setCompletedData', data);
  },
};

export default actions;
