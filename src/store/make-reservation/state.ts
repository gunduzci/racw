import { Vehicle } from 'src/models/search';
import { ExtraReservationInterface, DriverInfoReservationInterface, AdditionalAmountsReservationInterface } from 'src/models/makeReservation';

export interface MakeReservationStateInterface {
  currencyCode: string;
  vehicle: Vehicle | null;
  extras: ExtraReservationInterface[];
  extraAmount: number,
  additionalAmounts: AdditionalAmountsReservationInterface;
  rentalAmount: number,
  driverInfo: DriverInfoReservationInterface;
  paymentMethod: string;
  extraStepIsValid: boolean;
  driverStepIsValid: boolean;
  paymentStepIsValid: boolean;
  completed: {
    code: null | string;
    email: null | string;
    paymentMethodName: null | string;
    amount: null | number;
    currencyCode: null | string;
  };
}

function state(): MakeReservationStateInterface {
  return {
    currencyCode: 'EUR',
    vehicle: null,
    extras: [],
    extraAmount: 0,
    additionalAmounts: {
      country: 0,
      pickupPoint: 0,
      dropPoint: 0,
      drop: 0,
      closerDay: 0,
      total: 0,
    },
    rentalAmount: 0,
    driverInfo: {
      isValid: false,
      firstName: null,
      lastName: null,
      email: null,
      phone: null,
      country: null,
      licenseAge: null,
      birthDate: {
        day: null,
        month: null,
        year: null,
      }
    },
    paymentMethod: 'cash',
    extraStepIsValid: true,
    driverStepIsValid: false,
    paymentStepIsValid: true,
    completed: {
      code: null,
      email: null,
      paymentMethodName: null,
      amount: null,
      currencyCode: null,
    }
  }
}

export default state;
