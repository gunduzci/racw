import { store } from 'quasar/wrappers';
import { InjectionKey } from 'vue';
import { createStore, Store as VuexStore, useStore as vuexUseStore } from 'vuex';

import global from './module-global';
import { GlobalStateInterface } from './module-global/state';

import search from './search';
import { SearchStateInterface } from './search/state';

import makeReservation from './make-reservation';
import { MakeReservationStateInterface } from './make-reservation/state';

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export interface StateInterface {
  global: GlobalStateInterface;
  search: SearchStateInterface;
  makeReservation: MakeReservationStateInterface;
}

// provide typings for `this.$store`
declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $store: VuexStore<StateInterface>
  }
}

// provide typings for `useStore` helper
export const storeKey: InjectionKey<VuexStore<StateInterface>> = Symbol('vuex-key')

export default store(function (/* { ssrContext } */) {
  return createStore<StateInterface>({
    modules: {
      global,
      search,
      makeReservation,
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: !!process.env.DEBUGGING
  });
})

export function useStore() {
  return vuexUseStore(storeKey)
}
