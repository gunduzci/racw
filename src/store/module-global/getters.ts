import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { GlobalStateInterface } from './state';

const getters: GetterTree<GlobalStateInterface, StateInterface> = {
  getDeliveryPointTypeIcons: (state) => {
    return state.deliveryPointTypeIcons;
  }
};

export default getters;
