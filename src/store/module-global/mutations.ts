import { MutationTree } from 'vuex';
import { GlobalStateInterface, GlobalStateMetaInterface } from './state';

const mutation: MutationTree<GlobalStateInterface> = {
  setMeta (state: GlobalStateInterface, data: GlobalStateMetaInterface) {
    state.meta.title = data.title;
    state.meta.description = data.description;
    state.meta.keywords = data.keywords;
  },
};

export default mutation;
