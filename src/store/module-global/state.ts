export interface GlobalStateMetaInterface {
  title: string;
  description: string;
  keywords: string;
}

export interface GlobalStateInterface {
  deliveryPointTypeIcons: {
    [key: string]: string,
  };
  meta: GlobalStateMetaInterface;
}

function state(): GlobalStateInterface {
  return {
    deliveryPointTypeIcons: {
      airport: 'fas fa-plane',
      hotel: 'fas fa-hotel',
      office: 'fas fa-building',
      station: 'fas fa-bus-alt',
      address: 'fas fa-map-marked-alt'
    },
    meta: {
      title: '',
      description: '',
      keywords: '',
    }
  }
}

export default state;
