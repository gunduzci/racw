import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { GlobalStateInterface, GlobalStateMetaInterface } from './state';

const actions: ActionTree<GlobalStateInterface, StateInterface> = {
  setMeta ({ commit }, meta: GlobalStateMetaInterface) {
    commit('setMeta', meta);
  },
};

export default actions;
