import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import { SearchStateInterface } from './state';
import { SearchData } from 'src/models/search';

const actions: ActionTree<SearchStateInterface, StateInterface> = {
  fill ({ commit }, data: SearchData) {
    let pickupDateTimeString = null;
    let dropDateTimeString = null;
    let rentalDay = null;

    if (data.pickupDateTime !== null) {
      pickupDateTimeString = `${data.pickupDateTime.getFullYear()}-${('0' + (data.pickupDateTime.getMonth() + 1).toString()).slice(-2)}-${('0' + data.pickupDateTime.getDate().toString()).slice(-2)} ${('0' + data.pickupDateTime.getHours().toString()).slice(-2)}:${('0' + data.pickupDateTime.getMinutes().toString()).slice(-2)}`;
    }

    if (data.dropDateTime !== null) {
      dropDateTimeString = `${data.dropDateTime.getFullYear()}-${('0' + (data.dropDateTime.getMonth() + 1).toString()).slice(-2)}-${('0' + data.dropDateTime.getDate().toString()).slice(-2)} ${('0' + data.dropDateTime.getHours().toString()).slice(-2)}:${('0' + data.dropDateTime.getMinutes().toString()).slice(-2)}`;
    }

    if (data.pickupDateTime !== null && data.dropDateTime !== null) {
      const hourDiff: number = (data.dropDateTime.getTime() - data.pickupDateTime.getTime()) / 1000 / 60 / 60;
      rentalDay = Math.floor(hourDiff / 24);

      if (hourDiff % 24 > 3) {
        rentalDay++;
      }
    }

    commit('setPickupPoint', data.pickupPoint);
    commit('setDropPoint', data.dropPoint);
    commit('setDifferentReturnPoint', data.differentReturnPoint);
    commit('setPickupDateTime', data.pickupDateTime);
    commit('setPickupDateTimeString', pickupDateTimeString);
    commit('setDropDateTime', data.dropDateTime);
    commit('setDropDateTimeString', dropDateTimeString);
    commit('setRentalDay', rentalDay);
    commit('setIsReady');
  },

  setBackgroundImage ({ commit }, image: null | string) {
    commit('setBackgroundImage', image);
  },

  changeStatus ({ commit }, status: boolean) {
    commit('setOnSearching', status);
  },

  setMaxDateTime ({ commit }, maxDateTime: null | Date) {
    commit('setMaxDateTime', maxDateTime);
  },
};

export default actions;
