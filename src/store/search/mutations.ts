import { MutationTree } from 'vuex';
import { SearchStateInterface } from './state';
import { DeliveryPoint, Page } from 'src/models/search';

const mutation: MutationTree<SearchStateInterface> = {
  setIsReady (state: SearchStateInterface) {
    state.isReady = state.pickupPoint !== null && state.pickupDateTime !== null
      && state.dropDateTime !== null && (!state.differentReturnPoint || state.dropPoint !== null);
  },

  setPickupPoint (state: SearchStateInterface, point: DeliveryPoint) {
    state.pickupPoint = point === null || Object.keys(point).length === 0 ? null : point;
  },

  setDropPoint (state: SearchStateInterface, point: DeliveryPoint) {
    state.dropPoint = point === null || Object.keys(point).length === 0 ? null : point;
  },

  setDifferentReturnPoint (state: SearchStateInterface, status: boolean) {
    state.differentReturnPoint = status;
  },

  setPickupDateTime (state: SearchStateInterface, dateTime: Date) {
    state.pickupDateTime = dateTime;
  },

  setPickupDateTimeString (state: SearchStateInterface, dateTime: string) {
    state.pickupDateTimeString = dateTime;
  },

  setDropDateTime (state: SearchStateInterface, dateTime: Date) {
    state.dropDateTime = dateTime;
  },

  setDropDateTimeString (state: SearchStateInterface, dateTime: string) {
    state.dropDateTimeString = dateTime;
  },

  setRentalDay (state: SearchStateInterface, rentalDay: number) {
    state.rentalDay = rentalDay;
  },

  setOnSearching (state: SearchStateInterface, status: boolean) {
    state.onSearching = status;
  },

  setPage (state: SearchStateInterface, page: Page|null) {
    state.page = page;
  },

  setBackgroundImage (state: SearchStateInterface, image: null | string) {
    state.backgroundImage = image;
  },

  setMaxDateTime (state: SearchStateInterface, maxDateTime: null | Date) {
    state.maxDateTime = maxDateTime;
  },
};

export default mutation;
