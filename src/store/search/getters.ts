import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { SearchStateInterface } from './state';
import { SearchData } from 'src/models/search';

const getters: GetterTree<SearchStateInterface, StateInterface> = {
  getData: (state: SearchStateInterface): SearchData => {
    return <SearchData> {
      pickupPoint: state.pickupPoint,
      dropPoint: state.dropPoint,
      differentReturnPoint: state.differentReturnPoint,
      pickupDateTime: state.pickupDateTime,
      pickupDateTimeString: state.pickupDateTimeString,
      dropDateTime: state.dropDateTime,
      dropDateTimeString: state.dropDateTimeString,
      rentalDay: state.rentalDay,
    };
  }
};

export default getters;
