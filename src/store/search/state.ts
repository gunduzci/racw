import { DeliveryPoint } from 'src/models/search';
import { Page } from 'src/models/search';

export interface SearchStateInterface {
  isReady: boolean;
  pickupPoint: null | DeliveryPoint;
  dropPoint: null | DeliveryPoint;
  differentReturnPoint: boolean;
  pickupDateTime: null | Date;
  pickupDateTimeString: null | string;
  dropDateTime: null | Date;
  dropDateTimeString: null | string;
  rentalDay: null | number;
  onSearching: boolean;
  page: null | Page;
  backgroundImage: null | string;
  maxDateTime: null | Date;
}

function state(): SearchStateInterface {
  return {
    isReady: false,
    pickupPoint: null,
    dropPoint: null,
    differentReturnPoint: false,
    pickupDateTime: null,
    pickupDateTimeString: null,
    dropDateTime: null,
    dropDateTimeString: null,
    rentalDay: null,
    onSearching: false,
    page: null,
    backgroundImage: null,
    maxDateTime: null,
  }
}

export default state;
