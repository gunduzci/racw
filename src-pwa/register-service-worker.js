import { register } from 'register-service-worker'
import { Notify } from 'quasar'
import { mdiCached } from '@quasar/extras/mdi-v6'

// The ready(), registered(), cached(), updatefound() and updated()
// events passes a ServiceWorkerRegistration instance in their arguments.
// ServiceWorkerRegistration: https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration

register(process.env.SERVICE_WORKER_FILE, {
  // The registrationOptions object will be passed as the second argument
  // to ServiceWorkerContainer.register()
  // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerContainer/register#Parameter

  // registrationOptions: { scope: './' },

  ready (/* registration */) {
    // console.log('Service worker is active.')
  },

  registered (/* registration */) {
    // console.log('Service worker has been registered.')
  },

  cached (/* registration */) {
    // console.log('Content has been cached for offline use.')
  },

  updatefound (/* registration */) {
    // console.log('New content is downloading.')
  },

  updated (/* registration */) {
    Notify.create({
      color: 'negative',
      icon: mdiCached,
      message: 'Güncellenmiş içerik mevcut. Lütfen sayfayı yenileyin.',
      timeout: 0,
      multiLine: true,
      position: 'top',
      actions: [
        {
          label: 'Yenile',
          color: 'yellow',
          handler: () => {
            void navigator.serviceWorker.getRegistrations()
              .then(function(registrations) {
                for(let registration of registrations) {
                  void registration.unregister();
                }
              });

            setTimeout(() => {
              window.location.reload();
            }, 250);
          }
        },
        {
          label: 'Kapat',
          color: 'white',
          // eslint-disable-next-line @typescript-eslint/no-empty-function
          handler: () => {}
        }
      ]
    });
  },

  offline () {
    // Notify.create({
    //   color: 'negative',
    //   icon: mdiCached,
    //   message: 'İnternet bağlantısı bulunmuyor, uygulama çevrimdışı çalıştırılıyor.',
    //   timeout: 0,
    //   multiLine: true,
    //   position: 'top',
    //   actions: [
    //     {
    //       label: 'Kapat',
    //       color: 'white',
    //       // eslint-disable-next-line @typescript-eslint/no-empty-function
    //       handler: () => {}
    //     }
    //   ]
    // });
  },

  error (/* err */) {
    // console.error('Error during service worker registration:', err)
  }
})
