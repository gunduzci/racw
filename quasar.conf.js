/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */

// Configuration for your app
// https://v2.quasar.dev/quasar-cli/quasar-conf-js

/* eslint-env node */
/* eslint-disable @typescript-eslint/no-var-requires */
const { configure } = require('quasar/wrappers');

const preparePwaEnvFile = () => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const fs = require('fs');

  const keys = [
    'NODE_ENV',
    'APP_PORT',
    'APP_NAME',
    'APP_SHORTNAME',
    'APP_DESCRIPTION',
    'APP_DOMAIN',
    'APP_URL',
    'API_URL',
    'CDN_URL',
    'GTAG_ID',
    'GTM_ID',
    'PHONE_NUMBER',
    'ADDRESS',
  ];
  let rows = [];

  keys.forEach((key) => {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    rows.push(`  ${key}: '${process.env.hasOwnProperty(key) ? process.env[key] : ''}'`);
  });

  fs.writeFileSync('./src/helpers/pwa-env.js',
    `export default {
${rows.join(',\n')},
}
`);
}

module.exports = configure(function (ctx) {
  return {
    // https://v2.quasar.dev/quasar-cli/supporting-ts
    supportTS: {
      tsCheckerConfig: {
        eslint: {
          enabled: true,
          files: './src/**/*.{ts,tsx,js,jsx,vue}',
        },
      }
    },

    // https://v2.quasar.dev/quasar-cli/prefetch-feature
    // preFetch: true,

    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://v2.quasar.dev/quasar-cli/boot-files
    boot: [
      'i18n',
      'axios',
      // 'gtag',
      'gtm',
      // 'tawk-to',
    ],

    // https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: [
      'app.scss'
    ],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
      // 'mdi-v5',
      'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      // 'line-awesome',
      'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      // 'roboto-font', // optional, you are not bound to it
      'material-icons', // optional, you are not bound to it
    ],

    // Full list of options: https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
      webpackManifest: true,
      vueRouterMode: 'history', // available values: 'hash', 'history'

      env: require('dotenv').config().parsed,

      // transpile: false,

      // Add dependencies for transpiling with Babel (Array of string/regex)
      // (from node_modules, which are by default not transpiled).
      // Applies only if "transpile" is set to true.
      // transpileDependencies: [],

      // rtl: true, // https://v2.quasar.dev/options/rtl-support
      // preloadChunks: true,
      // showProgress: false,
      // gzip: true,
      // analyze: true,

      // Options below are automatically set depending on the env, set them if you want to override
      // extractCSS: false,

      // https://v2.quasar.dev/quasar-cli/handling-webpack
      // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
      // chainWebpack (/* chain */) {
      //   //
      // },
      chainWebpack (chain) {
        // const { WebpackPwaEnvPlugin } = require('./webpack-pwa-env-plugin.js');
        // chain.plugin('pwa-env').use(new WebpackPwaEnvPlugin);

        const webpack = require('webpack');
        chain.plugin('pwa-env-file-ignore').use(new webpack.WatchIgnorePlugin({ paths: ['./src/helpers/pwa-env.js'] }));
      },
      beforeDev () {
        preparePwaEnvFile();
      },
      beforeBuild () {
        preparePwaEnvFile();
      }
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      https: false,
      port: 8080,
      open: false, // opens browser window automatically
      // watchFiles: {
      //   ignored: [
      //     './src/helpers/pwa-env.js'
      //   ]
      // }
    },

    // https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      config: {
        lang: 'tr-TR',
        dark: false
      },

      // iconSet: 'material-icons', // Quasar icon set
      lang: 'tr', // Quasar language pack

      // For special cases outside of where the auto-import stategy can have an impact
      // (like functional components as one of the examples),
      // you can manually specify Quasar components/directives to be available everywhere:
      //
      // components: [],
      // directives: [],

      // Quasar plugins
      plugins: [
        'Dialog',
        'Notify',
        'Meta'
      ]
    },

    animations: 'all', // --- includes all animations
    // https://v2.quasar.dev/options/animations
    // animations: [],

    // https://v2.quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: true,

      // manualStoreHydration: true,
      // manualPostHydrationTrigger: true,

      prodPort: process.env.APP_PORT, // The default port that the production server should use
                      // (gets superseded if process.env.PORT is specified at runtime)

      maxAge: 1000 * 60 * 60 * 24 * 30,
        // Tell browser when a file from the server should expire from cache (in ms)

      middlewares: [
        ctx.prod ? 'compression' : '',
        'render' // keep this as last one
      ],

      // optional; add/remove/change properties
      // of production generated package.json
      extendPackageJson () { // pkg
        // directly change props of pkg;
        // no need to return anything
      },

      // optional;
      // handles the Webserver webpack config ONLY
      // which includes the SSR middleware
      extendWebpackWebserver () { // cfg
        // directly change props of cfg;
        // no need to return anything
      },

      // optional; EQUIVALENT to extendWebpack() but uses webpack-chain;
      // handles the Webserver webpack config ONLY
      // which includes the SSR middleware
      chainWebpackWebserver () { // chain
        // chain is a webpack-chain instance
        // of the Webpack configuration
      }
    },

    // https://v2.quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: 'GenerateSW', // 'GenerateSW' or 'InjectManifest'
      // workboxOptions: {
      //   // importScripts: [
      //   //   'sw-env.js'
      //   // ],
      // }, // only for GenerateSW

      // for the custom service worker ONLY (/src-pwa/custom-service-worker.[js|ts])
      // if using workbox in InjectManifest mode
      chainWebpackCustomSW (/* chain */) {
        //
      },

      manifest: {
        name: process.env.APP_NAME,
        short_name: process.env.APP_SHORTNAME,
        description: process.env.APP_DESCRIPTION,
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#6667AB',
        icons: [
          {
            // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
            src: process.env.CDN_URL + '/icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          },
          {
            // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
            src: process.env.CDN_URL + '/icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
            src: process.env.CDN_URL + '/icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
            src: process.env.CDN_URL + '/icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          },
          {
            // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
            src: process.env.CDN_URL + '/icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
    capacitor: {
      hideSplashscreen: true
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      bundler: 'packager', // 'packager' or 'builder'

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: 'racw'
      },

      // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
      chainWebpack (/* chain */) {
        // do something with the Electron main process Webpack cfg
        // extendWebpackMain also available besides this chainWebpackMain
      },

      // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
      chainWebpackPreload (/* chain */) {
        // do something with the Electron main process Webpack cfg
        // extendWebpackPreload also available besides this chainWebpackPreload
      },
    }
  }
});
